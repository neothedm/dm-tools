#!/usr/bin/python

import os, sys, urllib2, re, jsbeautifier, time

port_map = {}

TEST_FILE = "http://mirror.internode.on.net/pub/test/1meg.test"

def get_page_content(url):
	response = urllib2.urlopen(url);
	return response.read();

def get_port(port_str):
	port = ''
	for char in port_str:
		if not char == '+':
			port = port + port_map[char]
	return port

def get_js_file(data):
	m = re.search('<script src="([^"]*)"', data)
	if m == None:
		return
	return m.group(1)

def get_ip_list(data):
	ip_list = []	
	
	for m in re.finditer("(<script[^>]*>document.write\(\":\"([^\)]+)\)</script>)", data):
		data = data.replace(m.group(1), ":" + get_port(m.group(2)))

	data =  re.sub(r'<.+?>', '', data)
	for m in re.finditer("\d+\.\d+\.\d+\.\d+:\d+", data):
		ip_list.append(m.group())
	return ip_list
	
def load_port_map(ports):
	tmp_map = {}
	for line in ports.split("\n"):
		line = line.replace(";", "").replace("\n", "")
		if '=' in line:
			tmp = line.split("=")
			tmp_map[tmp[0].strip()] = tmp[1].strip()

	return tmp_map

	
def download_file(url, proxy):
	start = time.time()
	proxy_handler = urllib2.ProxyHandler({'http': 'http://' + proxy + '/'})
	proxy_auth_handler = urllib2.ProxyBasicAuthHandler()

	opener = urllib2.build_opener(proxy_handler, proxy_auth_handler)
	handler = opener.open(url);
	meta = handler.info()
	handler.read()
	size = float(meta.getheaders("Content-Length")[0]) / 1024
	seconds = (time.time() - start)
	return '{0:.2g}'.format(size / seconds)


def main():
	global port_map
	print "Getting proxy page ......................",
	data = get_page_content("http://www.samair.ru/proxy/")
	print "done"
	print "Getting port js file ....................",
	script = get_page_content("http://www.samair.ru" + get_js_file(data))
	print "done"
	print "Loading port map ........................",
	port_map = load_port_map(jsbeautifier.beautify(script))
	print "done"
	print "Parsing proxies .........................",
	ip_list = get_ip_list(data)
	print "done"
	print
	for ip in ip_list:
		print ip
	user_input = raw_input("Do you want to test these proxies' speed? [y/n]:")
	if not user_input == 'y':
		print "quiting..."
		return
	print "Using file", TEST_FILE, "to test proxies."
	for ip in ip_list:
		print "Testing proxy", ip, "\t\t\t",
		sys.stdout.flush()
		speed = download_file(TEST_FILE, ip)
		print speed, "kb/s"
	
	
main()
